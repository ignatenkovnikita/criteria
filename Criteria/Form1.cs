﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Criteria
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {

                dataGridView1.Columns.Add("1", textBox1.Text);
                textBox1.Text = "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataGridView t = dataGridView1;

            int a = 0;
            int  r = 0;
            for (int i = 0; i < t.Rows.Count; i++)
            {
                for (int j = 0; j < t.Columns.Count; j++)
                {
                    a = Convert.ToInt32(t.Rows[i].Cells[j].Value);
                    if (r < a)
                    {
                        r = a;
                    }
                }
            }
            //MessageBox.Show("Максимальное значение" + r.ToString);
            MessageBox.Show(string.Format("Максимальное значение {0}", r));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataGridView t = dataGridView1;

            int a=0;
            int r=0;
            for (int i = 0; i < t.Rows.Count; i++)
            {
                for (int j = 0; j < t.Columns.Count; j++)
                {
                    
                    a = Convert.ToInt32(t.Rows[i].Cells[j].Value);
                    if (i == 0 && j == 0) r = a;

                    if (a < r && a != 0)
                    {
                        r = a;
                    }
                }
            }
            //MessageBox.Show("Максимальное значение" + r.ToString);
            MessageBox.Show(string.Format("Максимальное значение {0}", r));

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
        
        private void button4_Click(object sender, EventArgs e)
        {
            DataGridView data = dataGridView1;
            DataGridView criteria = dataGridView2;
            if (data.Rows.Count == criteria.Rows.Count)
            {
                string res = "";
                for (int i = 0; i < data.Columns.Count; i++)
                {
                    double a = 0;
                    double d = 0;
                    double c = 0;

                    for (int j = 0; j < data.Rows.Count; j++ )
                    {
                        
                        d = Convert.ToInt32(data.Rows[j].Cells[i].Value);
                        c = Convert.ToInt32(criteria.Rows[j].Cells[1].Value);
                        if (c != 0)
                        {
                            a += d / c;
                        }

                    }
                    
                    res += string.Format("{0} - {1} \n", data.Columns[i].HeaderText, Math.Round(a, 2));
                    //res += data.Columns[i].HeaderText + " - " + a.ToString + "\n";
 
                }
                MessageBox.Show(string.Format("Сравнение: \n {0}",res));

            }
            else
            {
                MessageBox.Show("Количество строк в таблице Данные для сравнения и Критерии не одинаковы");
            }
        }
    }
}
